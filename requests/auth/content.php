<?php
# @*************************************************************************@
# @ @author Mansur Altamirov (Mansur_TL)                                    @
# @ @author_url 1: https://www.instagram.com/mansur_tl                      @
# @ @author_url 2: http://codecanyon.net/user/mansur_tl                     @
# @ @author_email: highexpresstore@gmail.com                                @
# @*************************************************************************@
# @ HighExpress - The Ultimate Modern Marketplace Platform                  @
# @ Copyright (c) 05.07.19 HighExpress. All rights reserved.                @
# @*************************************************************************@

//if ($_SESSION['locations_id']) {
//	hs_redirect('/');
//}
$hs['all_locations']    = get_locations_name('all');
$hs['all_locations']    = hs_get_locations_usage($hs['all_locations']);

if ($_GET['auth_page']=='sell_online'){
    if (empty($_SESSION['check_loged'])){
        hs_redirect('auth/login');
    }
}
if ($_GET['auth_page']=='location_verifications'){
    $ipGet=$_SERVER['REMOTE_ADDR'];
    $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ipGet));

    $cityName=strtolower($query['city']);

    //zimbabwe
    $cityData   = get_cityname($cityName);


    if (empty($cityData)){
        hs_redirect('auth/location_signup');
    }
}
if ($_GET['auth_page']=='continue'){

    setcookie("city_name", 2, time() + (10 * 365 * 24 * 60 * 60), '/') or die('unable to create cookie');
    setcookie("surburb_name", 'test', time() + (10 * 365 * 24 * 60 * 60), '/') or die('unable to create cookie');

    $me['is_logged'] = true;

    $_SESSION['check_loged'] = false;

    $_SESSION['locations_id'] =2;

    $_SESSION['lang']='english';
    $hs['language']       = fetch_or_get($_SESSION['lang'],'english');

//    print_r($_SESSION);
//    exit();

    hs_redirect('/');

}

$_SESSION['lang']='english';
$hs['language']       = fetch_or_get($_SESSION['lang'],'english');



$hs['auth_page']  =  fetch_or_get($_GET['auth_page'],'login');
$hs['page_title'] =  $hs['config']['title'];
$hs['page_desc']  =  $hs['config']['description'];
$hs['page_kw']    =  $hs['config']['keywords'];
$hs['pn']         =  'auth';
$hs['header_st']  =  false;
$hs['em_code']    =  (empty($_GET['em_code'])) ? null : $_GET['em_code'];
$hs['prods']      =  hs_get_preview_products(array(
	'limit'       => 15
));

$hs['prods']  = ((empty($hs['prods'])) ? array() : $hs['prods']);
$hs['ghosts'] = ((count($hs['prods']) < 15) ? range(1, (15 - count($hs['prods']))) : array());

if ($hs['auth_page'] == 'login') {
	$hs['first_item'] = (empty($hs['prods'][0])) ? array() : $hs['prods'][0];
	$hs['fi_sprice']  = (empty($hs['first_item']['fs_price']) ? hs_price('0.00') : $hs['first_item']['fs_price']);
    $hs['fi_rprice']  = (empty($hs['first_item']['fr_price']) ? hs_price('0.00') : $hs['first_item']['fr_price']);
}elseif ($hs['auth_page'] == 'location_verifications'){
    $hs['first_item'] = (empty($hs['prods'][0])) ? array() : $hs['prods'][0];
    $hs['fi_sprice']  = (empty($hs['first_item']['fs_price']) ? hs_price('0.00') : $hs['first_item']['fs_price']);
    $hs['fi_rprice']  = (empty($hs['first_item']['fr_price']) ? hs_price('0.00') : $hs['first_item']['fr_price']);
}

$hs['site_content'] = hs_loadpage('auth/content');
